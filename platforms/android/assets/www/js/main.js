// $(document).bind("mobileinit", function(){
//   $.extend(  $.mobile , {
//     'defaultPageTransition': 'slide'
//   });
// });

$(document).ready(function(){
    $(document).on("tabsbeforeactivate", "#tabs", function (e, ui) {
        var reverse = ui.newPanel.index() < ui.oldPanel.index() ? " reverse" : "",
            classes = "in slide" + reverse;
        $(ui.newPanel).addClass(classes).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            $(this).removeClass(classes);
        });
    });

    $.extend(  $.mobile , {
        'defaultPageTransition': 'slide'
    });

    $('#select-native-1').change(function(){
            var chapter = $(this).val();
            $('.parts_item').fadeOut(100);
            setTimeout(function() {
                $('#list_item_' + chapter).fadeIn();
            }, 110);
        }
    );

    $('#chapter_list a').click(function(){
        var part = $(this).data('part');
    });

    $("#tabs").on("tabbeforeactivate", function (e, ui) {
      $(ui.newPanel)
          .addClass("animation")
          .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        $(this).removeClass("animation");
      });
    });

    // Проверяет какой раздел выбран для тестирования и записывает его название в локалсторедж

    $('#begin_test_button').click(function(){ 
        var chapter = $("#testing input:checked").data('chapter');
        window.localStorage.setItem("chaptersListForTest", chapter);
    });
});