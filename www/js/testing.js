function beginTest(){ // Главная функция, формирует список вопросов и возвращает большой массив
    // Вынимаем список глав из хранилища
    var chapterList = window.localStorage.getItem("chaptersListForTest");
    makeChaptersList(); // Проблема, выполняется уже после рендеринга страницы, поэтому изменения применяются только после рефреша страницы
    qList = window.localStorage.getItem("questionListChapter_"+chapterList);
    alert(JSON.parse(qList)); // получили список id вопросов

}

function getRadio(arr){ // Передаю массив с ответами, возвращается список радиобаттонов, лишние баттоны скрываются
	$('#radio').show();
	for(i = 1; i <= arr.length; i++) {
	    $('#radio form label:nth-of-type('+ i +')').text(arr[i-1]);
	}

	for(i = 6; i > arr.length; i--) {
	    $('#radio form label:nth-of-type('+ i +'), #radio form input:nth-of-type('+ i +')').hide();
	}
}

function getCheckboxes(arr){ // Передаю массив с ответами, возвращается список чекбоксов, лишние чекбоксы скрываются
	$('#checkbox').show();
	for(i = 1; i <= arr.length; i++) {
	    $('#checkbox form label:nth-of-type('+ i +')').text(arr[i-1]);
	}

	for(i = 6; i > arr.length; i--) {
	    $('#checkbox form label:nth-of-type('+ i +'), #checkbox form input:nth-of-type('+ i +')').hide();
	}
}

function getTextField(){ // Показывает текстовое поле, если вопрос требует ввести ответ самостоятельно
	$('#text_field').show();
}

function getQuestionText(question){ // Передаешь текст вопроса, функция заполняет параграф в нужном месте
	$('#question').show();
	$('#question p').text(question);
}

function getCode(code){ // Передаешь текст кода, функция заполняет параграф в нужном месте
	$('#code').show();
	$('#code p').html(code);
}
beginTest();
// getQuestionText('Пример вопроса');
// getTextField();
// getCheckboxes(a);
// getRadio(a);