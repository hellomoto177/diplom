document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {

}

var db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
db.transaction(populateDB, errorCB, successCB);

function populateDB(tx) {
    // tx.executeSql('DROP TABLE IF EXISTS DEMO');
    // tx.executeSql('CREATE TABLE IF NOT EXISTS DEMO (id, data)');
    // tx.executeSql('INSERT INTO DEMO (data) VALUES ("First row")');
    // tx.executeSql('INSERT INTO DEMO (data) VALUES ("Second row")');
}

function errorCB(tx, err) {
    alert("Error processing SQL: "+err);
}

function successCB() {
    // alert("success!");
}

function beginTest() {
	db.transaction(addRow, errorCB);
}

function addRow(tx) {
	tx.executeSql('INSERT INTO DEMO (data) VALUES ("added row")');
}

