var db = window.openDatabase("Database", "1.0", "Testing", 200000);
db.transaction(createTables, errorCB, fillingTables);

function createTables(tx) {
	tx.executeSql('DROP TABLE IF EXISTS FIRST_START');
	tx.executeSql('CREATE TABLE FIRST_START (id unique, value)');
	tx.executeSql('INSERT INTO FIRST_START (id, value) VALUES (1,1)');
}

function fillingTables() {
	// console.log('Создание таблиц тестовой таблицы прошло успешно, создаем остальные');
	var db = window.openDatabase("Database", "1.0", "Testing", 200000);
	db.transaction(queriesForFirstStart, errorCB);
}

function successCB(tx, result) {
	// console.log(result);
}

function errorCB(err) {
    console.log("Error processing SQL: "+err.code);
}

// Заполняем локальное хранилище списком вопросов в каждой главе
function makeChaptersList(chap) {
	// console.log('Создаем новые листы вопросов по главам');
	// TODO: В инициализацию эту очистку
	// window.localStorage.setItem('questionListChapter_1', '');
	// window.localStorage.setItem('questionListChapter_2', '');
	// window.localStorage.setItem('questionListChapter_3', '');
	// window.localStorage.setItem('questionListChapter_4', '');
	// window.localStorage.setItem('questionListChapter_5', '');
	// window.localStorage.setItem('questionListChapter_6', '');
	// window.localStorage.setItem('questionListChapter_7', '');
	// window.localStorage.setItem('questionListChapter_8', '');

	db.transaction
	(
	   	function (tx){
	        tx.executeSql('SELECT id FROM Questions WHERE id_chapter IN (?)', [1], function(tx,results){
                var len = results.rows.length;
                ids_arr = Array();
                for(i = 0; i < len; i++) {
                	ids_arr[i] = results.rows.item(i)['id'];
                }
                var toDb = JSON.stringify(ids_arr);
                window.localStorage.setItem('questionListChapter_1', toDb);
            }, errorCB);

            tx.executeSql('SELECT id FROM Questions WHERE id_chapter IN (?)', [2], function(tx,results){
                var len = results.rows.length;
                ids_arr = Array();
                for(i = 0; i < len; i++) {
                	ids_arr[i] = results.rows.item(i)['id'];
                }
                var toDb = JSON.stringify(ids_arr);
                window.localStorage.setItem('questionListChapter_2', toDb);
            }, errorCB);

            tx.executeSql('SELECT id FROM Questions WHERE id_chapter IN (?)', [3], function(tx,results){
                var len = results.rows.length;
                ids_arr = Array();
                for(i = 0; i < len; i++) {
                	ids_arr[i] = results.rows.item(i)['id'];
                }
                var toDb = JSON.stringify(ids_arr);
                window.localStorage.setItem('questionListChapter_3', toDb);
            }, errorCB);

            tx.executeSql('SELECT id FROM Questions WHERE id_chapter IN (?)', [4], function(tx,results){
                var len = results.rows.length;
                ids_arr = Array();
                for(i = 0; i < len; i++) {
                	ids_arr[i] = results.rows.item(i)['id'];
                }
                var toDb = JSON.stringify(ids_arr);
                window.localStorage.setItem('questionListChapter_4', toDb);
            }, errorCB);

            tx.executeSql('SELECT id FROM Questions WHERE id_chapter IN (?)', [5], function(tx,results){
                var len = results.rows.length;
                ids_arr = Array();
                for(i = 0; i < len; i++) {
                	ids_arr[i] = results.rows.item(i)['id'];
                }
                var toDb = JSON.stringify(ids_arr);
                window.localStorage.setItem('questionListChapter_5', toDb);
            }, errorCB);

            tx.executeSql('SELECT id FROM Questions WHERE id_chapter IN (?)', [6], function(tx,results){
                var len = results.rows.length;
                ids_arr = Array();
                for(i = 0; i < len; i++) {
                	ids_arr[i] = results.rows.item(i)['id'];
                }
                var toDb = JSON.stringify(ids_arr);
                window.localStorage.setItem('questionListChapter_6', toDb);
            }, errorCB);

            tx.executeSql('SELECT id FROM Questions WHERE id_chapter IN (?)', [7], function(tx,results){
                var len = results.rows.length;
                ids_arr = Array();
                for(i = 0; i < len; i++) {
                	ids_arr[i] = results.rows.item(i)['id'];
                }
                var toDb = JSON.stringify(ids_arr);
                window.localStorage.setItem('questionListChapter_7', toDb);
            }, errorCB);

            tx.executeSql('SELECT id FROM Questions WHERE id_chapter IN (?)', [8], function(tx,results){
                var len = results.rows.length;
                ids_arr = Array();
                for(i = 0; i < len; i++) {
                	ids_arr[i] = results.rows.item(i)['id'];
                }
                var toDb = JSON.stringify(ids_arr);
                window.localStorage.setItem('questionListChapter_8', toDb);
            }, errorCB);
	   	},errorCB,successCB
	);
}

function queriesForFirstStart(tx) {
	// Создаем таблицы 
	tx.executeSql('DROP TABLE IF EXISTS DEMO');
	tx.executeSql('DROP TABLE IF EXISTS Questions');
	tx.executeSql('DROP TABLE IF EXISTS Answers');
	tx.executeSql('DROP TABLE IF EXISTS Correct');
	tx.executeSql('DROP TABLE IF EXISTS Chapters');

	tx.executeSql('CREATE TABLE IF NOT EXISTS Questions (id, id_chapter, question, code)');
	tx.executeSql('CREATE TABLE IF NOT EXISTS Answers (id, answer)');
	tx.executeSql('CREATE TABLE IF NOT EXISTS Correct (id, id_q, id_a)');
	tx.executeSql('CREATE TABLE IF NOT EXISTS Chapters (id unique, chapter)');
	tx.executeSql('CREATE TABLE IF NOT EXISTS Answer_format (id_question, format)');

	// Наполняем справочник форматов ответов
	tx.executeSql('INSERT INTO Answer_format (id_question, format) VALUES (1, "radio")');
	tx.executeSql('INSERT INTO Answer_format (id_question, format) VALUES (2, "radio")');
	// Наполняем справочник "Главы"
	tx.executeSql('INSERT INTO Chapters (id, chapter) VALUES (1, "Basics")');

	// Заполняем данные по первой главе

	// 1
	tx.executeSql('INSERT INTO Questions (id, id_chapter, question, code) VALUES (1, 1, "What is the output of the following code?", "echo &#39;1&#39; . (print &#39;2&#39;) + 3;")');
	tx.executeSql('INSERT INTO Answers (id, answer) VALUES (1, "123")');
	tx.executeSql('INSERT INTO Answers (id, answer) VALUES (2, "213")');
	tx.executeSql('INSERT INTO Answers (id, answer) VALUES (3, "142")');
	tx.executeSql('INSERT INTO Answers (id, answer) VALUES (4, "214")');
	tx.executeSql('INSERT INTO Answers (id, answer) VALUES (5, "Syntax error")');
	tx.executeSql('INSERT INTO Correct (id, id_q, id_a) VALUES (1, 1, 4)');

	// 2
	tx.executeSql('INSERT INTO Questions (id, id_chapter, question, code) VALUES (2, 1, "What is the output of the following code?", "$a = 3;<br /> switch ($a) {<br /> &nbsp;&nbsp; case 1: echo &#39;one&#39;; break;<br /> &nbsp;&nbsp; case 2: echo &#39;two&#39;; break;<br /> &nbsp;&nbsp; default: echo &#39;four&#39;; break;<br /> &nbsp;&nbsp; case 3: echo &#39;three&#39;; break;<br /> }")');
	tx.executeSql('INSERT INTO Answers (id, answer) VALUES (6, "one")');
	tx.executeSql('INSERT INTO Answers (id, answer) VALUES (7, "two")');
	tx.executeSql('INSERT INTO Answers (id, answer) VALUES (8, "three")');
	tx.executeSql('INSERT INTO Answers (id, answer) VALUES (9, "Four")');
	tx.executeSql('INSERT INTO Correct (id, id_q, id_a) VALUES (2, 2, 8)');
}




